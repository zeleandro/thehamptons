'use strict';

// Init the application configuration module for AngularJS application
var ApplicationConfiguration = (function() {
	// Init module configuration options
	var applicationModuleName = 'thehamptons';
	var applicationModuleVendorDependencies = ['ngResource', 'ngCookies',  'ngAnimate',  'ngTouch',  'ngSanitize',  'ui.router', 'ui.bootstrap', 'ui.utils', 'ng-dropdown-multiselect', 'ngAutocomplete'];

	// Add a new vertical module
	var registerModule = function(moduleName, dependencies) {
		// Create angular module
		angular.module(moduleName, dependencies || []);

		// Add the module to the AngularJS configuration file
		angular.module(applicationModuleName).requires.push(moduleName);
	};

	return {
		applicationModuleName: applicationModuleName,
		applicationModuleVendorDependencies: applicationModuleVendorDependencies,
		registerModule: registerModule
	};
})();
'use strict';

//Start by defining the main module and adding the module dependencies
angular.module(ApplicationConfiguration.applicationModuleName, ApplicationConfiguration.applicationModuleVendorDependencies);

// Setting HTML5 Location Mode
angular.module(ApplicationConfiguration.applicationModuleName).config(['$locationProvider',
	function($locationProvider) {
		$locationProvider.hashPrefix('!');
	}
]);

//Then define the init function for starting up the application
angular.element(document).ready(function() {
	//Fixing facebook bug with redirect
	if (window.location.hash === '#_=_') window.location.hash = '#!';

	//Then init the app
	angular.bootstrap(document, [ApplicationConfiguration.applicationModuleName]);
});
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('articles');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('clientes');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('core');
'use strict';

// Use Applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('users');
'use strict';

// Use applicaion configuration module to register a new module
ApplicationConfiguration.registerModule('ventas');
'use strict';

// Configuring the Articles module
angular.module('articles').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		//Menus.addMenuItem('topbar', 'Articles', 'articles', 'dropdown', '/articles(/create)?');
		//Menus.addSubMenuItem('topbar', 'articles', 'List Articles', 'articles');
		//Menus.addSubMenuItem('topbar', 'articles', 'New Article', 'articles/create');
	}
]);
'use strict';

// Setting up route
angular.module('articles').config(['$stateProvider',
	function($stateProvider) {
		// Articles state routing
		$stateProvider.
		state('listArticles', {
			url: '/articles',
			templateUrl: 'modules/articles/views/list-articles.client.view.html'
		}).
		state('createArticle', {
			url: '/articles/create',
			templateUrl: 'modules/articles/views/create-article.client.view.html'
		}).
		state('viewArticle', {
			url: '/articles/:articleId',
			templateUrl: 'modules/articles/views/view-article.client.view.html'
		}).
		state('editArticle', {
			url: '/articles/:articleId/edit',
			templateUrl: 'modules/articles/views/edit-article.client.view.html'
		});
	}
]);
'use strict';

angular.module('articles').controller('ArticlesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Articles',
	function($scope, $stateParams, $location, Authentication, Articles) {
		$scope.authentication = Authentication;

		$scope.create = function() {
			var article = new Articles({
				title: this.title,
				content: this.content
			});
			article.$save(function(response) {
				$location.path('articles/' + response._id);

				$scope.title = '';
				$scope.content = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.remove = function(article) {
			if (article) {
				article.$remove();

				for (var i in $scope.articles) {
					if ($scope.articles[i] === article) {
						$scope.articles.splice(i, 1);
					}
				}
			} else {
				$scope.article.$remove(function() {
					$location.path('articles');
				});
			}
		};

		$scope.update = function() {
			var article = $scope.article;

			article.$update(function() {
				$location.path('articles/' + article._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		$scope.find = function() {
			$scope.articles = Articles.query();
		};

		$scope.findOne = function() {
			$scope.article = Articles.get({
				articleId: $stateParams.articleId
			});
		};
	}
]);
'use strict';

//Articles service used for communicating with the articles REST endpoints
angular.module('articles').factory('Articles', ['$resource',
	function($resource) {
		return $resource('articles/:articleId', {
			articleId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Configuring the Articles module
angular.module('clientes').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Clientes', 'clientes', 'dropdown', '/clientes(/create)?');
		Menus.addSubMenuItem('topbar', 'clientes', 'Lista de Clientes', 'clientes');
		Menus.addSubMenuItem('topbar', 'clientes', 'Nuevo Cliente', 'clientes/create');
	}
]);
'use strict';

//Setting up route
angular.module('clientes').config(['$stateProvider',
	function($stateProvider) {
		// Clientes state routing
		$stateProvider.
		state('listClientes', {
			url: '/clientes',
			templateUrl: 'modules/clientes/views/list-clientes.client.view.html'
		}).
		state('createCliente', {
			url: '/clientes/create',
			templateUrl: 'modules/clientes/views/create-cliente.client.view.html'
		}).
		state('viewCliente', {
			url: '/clientes/:clienteId',
			templateUrl: 'modules/clientes/views/view-cliente.client.view.html'
		}).
		state('editCliente', {
			url: '/clientes/:clienteId/edit',
			templateUrl: 'modules/clientes/views/edit-cliente.client.view.html'
		});
	}
]);
'use strict';

// Clientes controller
angular.module('clientes').controller('ClientesController', ['$scope', '$stateParams', '$location', '$http', 'Authentication', 'Clientes',
	function($scope, $stateParams, $location, $http, Authentication, Clientes) {
		$scope.authentication = Authentication;
		$scope.marcasModel = []; //Modelo de marcas seleccionadas

		//Cargar las ventas del cliente
		if ($stateParams.clienteId) 
		{
			$http.get('/ventasCliente/' + $stateParams.clienteId)
			.success(function(ventas) {
				$scope.ventas = ventas;
			});
		}

		// Create new Cliente
		$scope.create = function() {
			// Create new Cliente object
			var cliente = new Clientes ({
				nombre: this.nombre,
				telefono: this.telefono,
				email: this.email,
				direccion: this.direccion,
				nacimiento: this.nacimiento,
				talleRemera: this.talleRemera,
				talleJean: this.talleJean,
				observaciones: this.observaciones,
				intereses: this.marcasModel,
				ciudad: this.ciudad
			});

			// Redirect after save
			cliente.$save(function(response) {
				$location.path('clientes/' + response._id);

				// Clear form fields
				$scope.nombre = '';
				$scope.telefono = '';
				$scope.email = '';
				$scope.nacimiento = '';
				$scope.direccion = '';
				$scope.talleRemera = '';
				$scope.talleJean = '';
				$scope.observaciones = '';
				$scope.intereses = '';
				$scope.ciudad = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Cliente
		$scope.remove = function(cliente) {
			if ( cliente ) { 
				cliente.$remove();

				for (var i in $scope.clientes) {
					if ($scope.clientes [i] === cliente) {
						$scope.clientes.splice(i, 1);
					}
				}
			} else {
				$scope.cliente.$remove(function() {
					$location.path('clientes');
				});
			}
		};

		// Update existing Cliente
		$scope.update = function() {
			var cliente = $scope.cliente;

			cliente.$update(function() {
				$location.path('clientes/' + cliente._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Clientes
		$scope.find = function() {
			$scope.clientes = Clientes.query();
		};

		// Find existing Cliente
		$scope.findOne = function() {
			$scope.cliente = Clientes.get({ 
				clienteId: $stateParams.clienteId
			});
		};

	    $scope.cityResult = '';
	    $scope.cityOptions = {
	      country: 'ar',
	      types: '(cities)'
	    };
	    $scope.cityDetails = '';


	}
]);
'use strict';

angular.module('clientes').filter('clienteFilter', [
	function() {
		return function(input) {
			// Cliente filter directive logic
			// ...

			return 'clienteFilter filter: ' + input;
		};
	}
]);
'use strict';

//Clientes service used to communicate Clientes REST endpoints
angular.module('clientes').factory('Clientes', ['$resource',
	function($resource) {
		return $resource('clientes/:clienteId', { clienteId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Setting up route
angular.module('core').config(['$stateProvider', '$urlRouterProvider',
	function($stateProvider, $urlRouterProvider) {
		// Redirect to home view when route not found
		$urlRouterProvider.otherwise('/');

		// Home state routing
		$stateProvider.
		state('marcas', {
			url: '/marcas',
			templateUrl: 'modules/core/views/marcas.client.view.html'
		}).
		state('home', {
			url: '/',
			templateUrl: 'modules/core/views/home.client.view.html'
		});
	}
]);
'use strict';

angular.module('core').controller('HeaderController', ['$scope', 'Authentication', 'Menus',
	function($scope, Authentication, Menus) {
		$scope.authentication = Authentication;
		$scope.isCollapsed = false;
		$scope.menu = Menus.getMenu('topbar');

		$scope.toggleCollapsibleMenu = function() {
			$scope.isCollapsed = !$scope.isCollapsed;
		};

		// Collapsing the menu after navigation
		$scope.$on('$stateChangeSuccess', function() {
			$scope.isCollapsed = false;
		});
	}
]);
'use strict';


angular.module('core').controller('HomeController', ['$scope', 'Authentication',
	function($scope, Authentication) {
		// This provides Authentication context.
		$scope.authentication = Authentication;
	}
]);
'use strict';

angular.module('core').controller('MarcasController', ['$scope', '$location', '$http',
	function($scope, $location, $http) {
		var marcas = [];
		$http.get('/marcas')
		.success(function(marcasList) {
			$scope.marcas = marcasList;
		});


		$scope.marcasSettings = {
			displayProp: 'nombre', 
			idProp: 'nombre', 
			externalIdProp: 'nombre',
			smartButtonMaxItems: 10
		};

	}
]);
'use strict';

angular.module('core').controller('TiposController', ['$scope', '$location', '$http',
	function($scope, $location, $http) {
		$http.get('/tipos')
		.success(function(tiposList) {
			$scope.tipos = tiposList;
		});	}
]);
'use strict';

//Menu service used for managing  menus
angular.module('core').service('Menus', [

	function() {
		// Define a set of default roles
		this.defaultRoles = ['*'];

		// Define the menus object
		this.menus = {};

		// A private function for rendering decision 
		var shouldRender = function(user) {
			if (user) {
				if (!!~this.roles.indexOf('*')) {
					return true;
				} else {
					for (var userRoleIndex in user.roles) {
						for (var roleIndex in this.roles) {
							if (this.roles[roleIndex] === user.roles[userRoleIndex]) {
								return true;
							}
						}
					}
				}
			} else {
				return this.isPublic;
			}

			return false;
		};

		// Validate menu existance
		this.validateMenuExistance = function(menuId) {
			if (menuId && menuId.length) {
				if (this.menus[menuId]) {
					return true;
				} else {
					throw new Error('Menu does not exists');
				}
			} else {
				throw new Error('MenuId was not provided');
			}

			return false;
		};

		// Get the menu object by menu id
		this.getMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			return this.menus[menuId];
		};

		// Add new menu object by menu id
		this.addMenu = function(menuId, isPublic, roles) {
			// Create the new menu
			this.menus[menuId] = {
				isPublic: isPublic || false,
				roles: roles || this.defaultRoles,
				items: [],
				shouldRender: shouldRender
			};

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenu = function(menuId) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Return the menu object
			delete this.menus[menuId];
		};

		// Add menu item object
		this.addMenuItem = function(menuId, menuItemTitle, menuItemURL, menuItemType, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Push new menu item
			this.menus[menuId].items.push({
				title: menuItemTitle,
				link: menuItemURL,
				menuItemType: menuItemType || 'item',
				menuItemClass: menuItemType,
				uiRoute: menuItemUIRoute || ('/' + menuItemURL),
				isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].isPublic : isPublic),
				roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].roles : roles),
				position: position || 0,
				items: [],
				shouldRender: shouldRender
			});

			// Return the menu object
			return this.menus[menuId];
		};

		// Add submenu item object
		this.addSubMenuItem = function(menuId, rootMenuItemURL, menuItemTitle, menuItemURL, menuItemUIRoute, isPublic, roles, position) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === rootMenuItemURL) {
					// Push new submenu item
					this.menus[menuId].items[itemIndex].items.push({
						title: menuItemTitle,
						link: menuItemURL,
						uiRoute: menuItemUIRoute || ('/' + menuItemURL),
						isPublic: ((isPublic === null || typeof isPublic === 'undefined') ? this.menus[menuId].items[itemIndex].isPublic : isPublic),
						roles: ((roles === null || typeof roles === 'undefined') ? this.menus[menuId].items[itemIndex].roles : roles),
						position: position || 0,
						shouldRender: shouldRender
					});
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeMenuItem = function(menuId, menuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				if (this.menus[menuId].items[itemIndex].link === menuItemURL) {
					this.menus[menuId].items.splice(itemIndex, 1);
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		// Remove existing menu object by menu id
		this.removeSubMenuItem = function(menuId, submenuItemURL) {
			// Validate that the menu exists
			this.validateMenuExistance(menuId);

			// Search for menu item to remove
			for (var itemIndex in this.menus[menuId].items) {
				for (var subitemIndex in this.menus[menuId].items[itemIndex].items) {
					if (this.menus[menuId].items[itemIndex].items[subitemIndex].link === submenuItemURL) {
						this.menus[menuId].items[itemIndex].items.splice(subitemIndex, 1);
					}
				}
			}

			// Return the menu object
			return this.menus[menuId];
		};

		//Adding the topbar menu
		this.addMenu('topbar');
	}
]);
'use strict';

// Config HTTP Error Handling
angular.module('users').config(['$httpProvider',
	function($httpProvider) {
		// Set the httpProvider "not authorized" interceptor
		$httpProvider.interceptors.push(['$q', '$location', 'Authentication',
			function($q, $location, Authentication) {
				return {
					responseError: function(rejection) {
						switch (rejection.status) {
							case 401:
								// Deauthenticate the global user
								Authentication.user = null;

								// Redirect to signin page
								$location.path('signin');
								break;
							case 403:
								// Add unauthorized behaviour 
								break;
						}

						return $q.reject(rejection);
					}
				};
			}
		]);
	}
]);
'use strict';

// Setting up route
angular.module('users').config(['$stateProvider',
	function($stateProvider) {
		// Users state routing
		$stateProvider.
		state('profile', {
			url: '/settings/profile',
			templateUrl: 'modules/users/views/settings/edit-profile.client.view.html'
		}).
		state('password', {
			url: '/settings/password',
			templateUrl: 'modules/users/views/settings/change-password.client.view.html'
		}).
		state('accounts', {
			url: '/settings/accounts',
			templateUrl: 'modules/users/views/settings/social-accounts.client.view.html'
		}).
		state('signup', {
			url: '/signup',
			templateUrl: 'modules/users/views/authentication/signup.client.view.html'
		}).
		state('signin', {
			url: '/signin',
			templateUrl: 'modules/users/views/authentication/signin.client.view.html'
		}).
		state('forgot', {
			url: '/password/forgot',
			templateUrl: 'modules/users/views/password/forgot-password.client.view.html'
		}).
		state('reset-invalid', {
			url: '/password/reset/invalid',
			templateUrl: 'modules/users/views/password/reset-password-invalid.client.view.html'
		}).
		state('reset-success', {
			url: '/password/reset/success',
			templateUrl: 'modules/users/views/password/reset-password-success.client.view.html'
		}).
		state('reset', {
			url: '/password/reset/:token',
			templateUrl: 'modules/users/views/password/reset-password.client.view.html'
		});
	}
]);
'use strict';

angular.module('users').controller('AuthenticationController', ['$scope', '$http', '$location', 'Authentication',
	function($scope, $http, $location, Authentication) {
		$scope.authentication = Authentication;

		// If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		$scope.signup = function() {
			$http.post('/auth/signup', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		$scope.signin = function() {
			$http.post('/auth/signin', $scope.credentials).success(function(response) {
				// If successful we assign the response to the global user model
				$scope.authentication.user = response;

				// And redirect to the index page
				$location.path('/');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);
'use strict';

angular.module('users').controller('PasswordController', ['$scope', '$stateParams', '$http', '$location', 'Authentication',
	function($scope, $stateParams, $http, $location, Authentication) {
		$scope.authentication = Authentication;

		//If user is signed in then redirect back home
		if ($scope.authentication.user) $location.path('/');

		// Submit forgotten password account id
		$scope.askForPasswordReset = function() {
			$scope.success = $scope.error = null;

			$http.post('/auth/forgot', $scope.credentials).success(function(response) {
				// Show user success message and clear form
				$scope.credentials = null;
				$scope.success = response.message;

			}).error(function(response) {
				// Show user error message and clear form
				$scope.credentials = null;
				$scope.error = response.message;
			});
		};

		// Change user password
		$scope.resetUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/auth/reset/' + $stateParams.token, $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.passwordDetails = null;

				// Attach user profile
				Authentication.user = response;

				// And redirect to the index page
				$location.path('/password/reset/success');
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);
'use strict';

angular.module('users').controller('SettingsController', ['$scope', '$http', '$location', 'Users', 'Authentication',
	function($scope, $http, $location, Users, Authentication) {
		$scope.user = Authentication.user;

		// If user is not signed in then redirect back home
		if (!$scope.user) $location.path('/');

		// Check if there are additional accounts 
		$scope.hasConnectedAdditionalSocialAccounts = function(provider) {
			for (var i in $scope.user.additionalProvidersData) {
				return true;
			}

			return false;
		};

		// Check if provider is already in use with current user
		$scope.isConnectedSocialAccount = function(provider) {
			return $scope.user.provider === provider || ($scope.user.additionalProvidersData && $scope.user.additionalProvidersData[provider]);
		};

		// Remove a user social account
		$scope.removeUserSocialAccount = function(provider) {
			$scope.success = $scope.error = null;

			$http.delete('/users/accounts', {
				params: {
					provider: provider
				}
			}).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.user = Authentication.user = response;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};

		// Update a user profile
		$scope.updateUserProfile = function(isValid) {
			if (isValid) {
				$scope.success = $scope.error = null;
				var user = new Users($scope.user);

				user.$update(function(response) {
					$scope.success = true;
					Authentication.user = response;
				}, function(response) {
					$scope.error = response.data.message;
				});
			} else {
				$scope.submitted = true;
			}
		};

		// Change user password
		$scope.changeUserPassword = function() {
			$scope.success = $scope.error = null;

			$http.post('/users/password', $scope.passwordDetails).success(function(response) {
				// If successful show success message and clear form
				$scope.success = true;
				$scope.passwordDetails = null;
			}).error(function(response) {
				$scope.error = response.message;
			});
		};
	}
]);
'use strict';

// Authentication service for user variables
angular.module('users').factory('Authentication', [
	function() {
		var _this = this;

		_this._data = {
			user: window.user
		};

		return _this._data;
	}
]);
'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
	function($resource) {
		return $resource('users', {}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);
'use strict';

// Configuring the Articles module
angular.module('ventas').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Ventas', 'ventas', 'dropdown', '/ventas(/create)?');
		Menus.addSubMenuItem('topbar', 'ventas', 'Listado de Ventas', 'ventas');
		Menus.addSubMenuItem('topbar', 'ventas', 'Nueva Venta', 'ventas/create');
	}
]);
'use strict';

//Setting up route
angular.module('ventas').config(['$stateProvider',
	function($stateProvider) {
		// Ventas state routing
		$stateProvider.
		state('listVentas', {
			url: '/ventas',
			templateUrl: 'modules/ventas/views/list-ventas.client.view.html'
		}).
		state('createVenta', {
			url: '/ventas/create',
			templateUrl: 'modules/ventas/views/create-venta.client.view.html'
		}).
		state('viewVenta', {
			url: '/ventas/:ventaId',
			templateUrl: 'modules/ventas/views/view-venta.client.view.html'
		}).
		state('editVenta', {
			url: '/ventas/:ventaId/edit',
			templateUrl: 'modules/ventas/views/edit-venta.client.view.html'
		}).
		state('createVentaCliente', {
			url: '/ventas/create/:clienteId',
			templateUrl: 'modules/ventas/views/create-venta.client.view.html'
		});
	}
]);
'use strict';

// Ventas controller
angular.module('ventas').controller('VentasController', ['$scope', '$stateParams', '$location', '$http', 'Authentication', 'Ventas',
	function($scope, $stateParams, $location, $http, Authentication, Ventas) {
		$scope.authentication = Authentication;

		//Si es una modificación no crear una nueva venta
		if (!$stateParams.ventaId)
		{
			$scope.venta = new Ventas({
				fecha: moment().format('YYYY-MM-DD'),
				formapago: 'Efectivo',
				descuento: 0,
				items:[{articulo:'', marca:'Seleccione', tipo: '', cantidad:1, precio:0}]
			});				
		}

		//Si viene desde cliente cargar el _id de cliente y nombre
		if ($stateParams.clienteId) 
		{
			$http.get('/clientes/' + $stateParams.clienteId)
			.success(function(client) {
				$scope.venta.cliente = {_id: client._id, nombre: client.nombre};
			});			
		}

		//Cargar clientes
		var arr_cli = [];
		$http.get('/clientes')
		.success(function(clientes) {
		for (var item in clientes) {
		    arr_cli.push(clientes[item]);
		}
			$scope.clientes = arr_cli;
		});

		//Cargar tipos de prendas
		var arr = [];
		$http.get('/tipos')
		.success(function(tipos) {
		for (var item in tipos) {
		    arr.push(tipos[item]);
		}
			$scope.tipos = arr;
		});

		// Create new Venta
		$scope.create = function() {
			var venta = this.venta;
			venta.total = $scope.calcular_total();

			// Redirect after save
			venta.$save(function(response) {
				$location.path('ventas/' + response._id);

				// Clear form fields
				$scope.fecha = '';
				$scope.formapago = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Venta
		$scope.remove = function(venta) {
			if ( venta ) { 
				venta.$remove();

				for (var i in $scope.ventas) {
					if ($scope.ventas [i] === venta) {
						$scope.ventas.splice(i, 1);
					}
				}
			} else {
				$scope.venta.$remove(function() {
					$location.path('ventas');
				});
			}
		};

		// Update existing Venta
		$scope.update = function() {
			var venta = $scope.venta;
			venta.total = $scope.calcular_total();

			venta.$update(function() {
				$location.path('ventas/' + venta._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Ventas
		$scope.find = function() {
			$scope.ventas = Ventas.query();
		};

		// Find existing Venta
		$scope.findOne = function() {
			$scope.venta = Ventas.get({ 
				ventaId: $stateParams.ventaId
			});
		};

		$scope.formaPagoModel = [{
			  name: 'Efectivo'
			}, {
			  name: 'Tarjeta'
			}, {
			  name: 'Cuenta Corriente'
		}];

		//Funciones de la linea de factura
		$scope.addItem = function() {
        	$scope.venta.items.push({articulo:'', marca:'', tipo:'', cantidad:1, precio:0});    
    	};

    	$scope.removeItem = function(item) {
        	$scope.venta.items.splice($scope.venta.items.indexOf(item), 1);    
    	};

		$scope.calcular_sub_total = function() {
		    var total = 0.00;
		    angular.forEach($scope.venta.items, function(item, key){
		      total += (item.cantidad * item.precio);
		    });
		    return total;
		};

		$scope.calcular_descuento = function() {
		    return (($scope.venta.descuento * $scope.calcular_sub_total())/100);
		};

		$scope.calcular_total = function() {
		    return $scope.calcular_sub_total() -  $scope.calcular_descuento();
		};

		//Funcion onSelect Cliente. Asigna el _id de Cliente a la venta seleccionada
		$scope.onSelect = function ($item, $model, $label) {
			$scope.venta.cliente._id = $item._id;
		};

	}

]);
'use strict';

angular.module('ventas').directive('autoComplete', [
	function() {
		return {
			template: '<div></div>',
			restrict: 'E',
			link: function postLink(scope, element, attrs) {
				// Auto complete directive logic
				// ...

				element.text('this is the autoComplete directive');
			}
		};
	}
]);
'use strict';

//Ventas service used to communicate Ventas REST endpoints
angular.module('ventas').factory('Ventas', ['$resource',
	function($resource) {
		return $resource('ventas/:ventaId', { ventaId: '@_id'
		}, {
			update: {
				method: 'PUT'
			}
		});
	}
]);