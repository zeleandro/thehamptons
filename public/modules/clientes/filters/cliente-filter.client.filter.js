'use strict';

angular.module('clientes').filter('clienteFilter', [
	function() {
		return function(input) {
			// Cliente filter directive logic
			// ...

			return 'clienteFilter filter: ' + input;
		};
	}
]);