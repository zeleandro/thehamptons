'use strict';

angular.module('ventas').directive('autoComplete', [
	function() {
		return {
			template: '<div></div>',
			restrict: 'E',
			link: function postLink(scope, element, attrs) {
				// Auto complete directive logic
				// ...

				element.text('this is the autoComplete directive');
			}
		};
	}
]);