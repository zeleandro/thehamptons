'use strict';

// Configuring the Articles module
angular.module('ventas').run(['Menus',
	function(Menus) {
		// Set top bar menu items
		Menus.addMenuItem('topbar', 'Ventas', 'ventas', 'dropdown', '/ventas(/create)?');
		Menus.addSubMenuItem('topbar', 'ventas', 'Listado de Ventas', 'ventas');
		Menus.addSubMenuItem('topbar', 'ventas', 'Nueva Venta', 'ventas/create');
	}
]);