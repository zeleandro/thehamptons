'use strict';

// Ventas controller
angular.module('ventas').controller('VentasController', ['$scope', '$stateParams', '$location', '$http', 'Authentication', 'Ventas',
	function($scope, $stateParams, $location, $http, Authentication, Ventas) {
		$scope.authentication = Authentication;

		//Si es una modificación no crear una nueva venta
		if (!$stateParams.ventaId)
		{
			$scope.venta = new Ventas({
				fecha: moment().format('YYYY-MM-DD'),
				formapago: 'Efectivo',
				descuento: 0,
				items:[{articulo:'', marca:'Seleccione', tipo: '', cantidad:1, precio:0}]
			});				
		}

		//Si viene desde cliente cargar el _id de cliente y nombre
		if ($stateParams.clienteId) 
		{
			$http.get('/clientes/' + $stateParams.clienteId)
			.success(function(client) {
				$scope.venta.cliente = {_id: client._id, nombre: client.nombre};
			});			
		}

		//Cargar clientes
		var arr_cli = [];
		$http.get('/clientes')
		.success(function(clientes) {
		for (var item in clientes) {
		    arr_cli.push(clientes[item]);
		}
			$scope.clientes = arr_cli;
		});

		//Cargar tipos de prendas
		var arr = [];
		$http.get('/tipos')
		.success(function(tipos) {
		for (var item in tipos) {
		    arr.push(tipos[item]);
		}
			$scope.tipos = arr;
		});

		// Create new Venta
		$scope.create = function() {
			var venta = this.venta;
			venta.total = $scope.calcular_total();

			// Redirect after save
			venta.$save(function(response) {
				$location.path('ventas/' + response._id);

				// Clear form fields
				$scope.fecha = '';
				$scope.formapago = '';
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing Venta
		$scope.remove = function(venta) {
			if ( venta ) { 
				venta.$remove();

				for (var i in $scope.ventas) {
					if ($scope.ventas [i] === venta) {
						$scope.ventas.splice(i, 1);
					}
				}
			} else {
				$scope.venta.$remove(function() {
					$location.path('ventas');
				});
			}
		};

		// Update existing Venta
		$scope.update = function() {
			var venta = $scope.venta;
			venta.total = $scope.calcular_total();

			venta.$update(function() {
				$location.path('ventas/' + venta._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Ventas
		$scope.find = function() {
			$scope.ventas = Ventas.query();
		};

		// Find existing Venta
		$scope.findOne = function() {
			$scope.venta = Ventas.get({ 
				ventaId: $stateParams.ventaId
			});
		};

		$scope.formaPagoModel = [{
			  name: 'Efectivo'
			}, {
			  name: 'Tarjeta'
			}, {
			  name: 'Cuenta Corriente'
		}];

		//Funciones de la linea de factura
		$scope.addItem = function() {
        	$scope.venta.items.push({articulo:'', marca:'', tipo:'', cantidad:1, precio:0});    
    	};

    	$scope.removeItem = function(item) {
        	$scope.venta.items.splice($scope.venta.items.indexOf(item), 1);    
    	};

		$scope.calcular_sub_total = function() {
		    var total = 0.00;
		    angular.forEach($scope.venta.items, function(item, key){
		      total += (item.cantidad * item.precio);
		    });
		    return total;
		};

		$scope.calcular_descuento = function() {
		    return (($scope.venta.descuento * $scope.calcular_sub_total())/100);
		};

		$scope.calcular_total = function() {
		    return $scope.calcular_sub_total() -  $scope.calcular_descuento();
		};

		//Funcion onSelect Cliente. Asigna el _id de Cliente a la venta seleccionada
		$scope.onSelect = function ($item, $model, $label) {
			$scope.venta.cliente._id = $item._id;
		};

	}

]);