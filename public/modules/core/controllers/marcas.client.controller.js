'use strict';

angular.module('core').controller('MarcasController', ['$scope', '$location', '$http',
	function($scope, $location, $http) {
		var marcas = [];
		$http.get('/marcas')
		.success(function(marcasList) {
			$scope.marcas = marcasList;
		});


		$scope.marcasSettings = {
			displayProp: 'nombre', 
			idProp: 'nombre', 
			externalIdProp: 'nombre',
			smartButtonMaxItems: 10
		};

	}
]);