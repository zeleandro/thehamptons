'use strict';

angular.module('core').controller('TiposController', ['$scope', '$location', '$http',
	function($scope, $location, $http) {
		$http.get('/tipos')
		.success(function(tiposList) {
			$scope.tipos = tiposList;
		});	}
]);