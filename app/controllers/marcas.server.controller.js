'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Marca = mongoose.model('Marca'),
    _ = require('lodash');

/**
 * Create a Marca
 */
exports.create = function(req, res) {

};

/**
 * Show the current Marca
 */
exports.read = function(req, res) {

};

/**
 * Update a Marca
 */
exports.update = function(req, res) {

};

/**
 * Delete an Marca
 */
exports.delete = function(req, res) {

};

/**
 * List of Marcas
 */
exports.list = function(req, res) {
	Marca.find().exec(function(err, marcas) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(marcas);
		}
	});
};