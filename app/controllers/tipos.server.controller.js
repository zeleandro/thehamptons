'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	errorHandler = require('./errors.server.controller'),
	Tipo = mongoose.model('Tipo'),
    _ = require('lodash');

/**
 * Create a Tipo
 */
exports.create = function(req, res) {

};

/**
 * Show the current Tipo
 */
exports.read = function(req, res) {

};

/**
 * Update a Tipo
 */
exports.update = function(req, res) {

};

/**
 * Delete an Tipo
 */
exports.delete = function(req, res) {

};

/**
 * List of Tipos
 */
exports.list = function(req, res) {
	Tipo.find().exec(function(err, tipos) {
		if (err) {
			return res.status(400).send({
				message: errorHandler.getErrorMessage(err)
			});
		} else {
			res.jsonp(tipos);
		}
	});
};