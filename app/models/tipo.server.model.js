'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Tipos Schema
 */
var TipoSchema = new Schema({
	nombre: {
		type: String,
		trim: true
	},
	genero: {
		type: String,
		trim: true
	}
});

mongoose.model('Tipo', TipoSchema);