'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

var ItemSchema = new Schema({
	articulo: {
		type: String,
		default: '',
		trim: true
	},
	cantidad: {
		type: Number
	},
	precio: {
		type: Number
	},
	tipo: {
		type: String,
		trim: true
	},
	marca: {
		type: String,
		trim: true
	}
});

/**
 * Venta Schema
 */
var VentaSchema = new Schema({
	fecha: {
		type: String,
		required: 'Ingrese una fecha',
		trim: true
	},
	formapago: {
		type: String,
		default: '',
		trim: true
	},
	cliente: {
		_id: {
			type: Schema.ObjectId
		},
		nombre: {
			type: String
		}
	},
	items: {
		type: [ItemSchema]
	},
	descuento: {
		type: Number
	},
	total: {
		type: Number
	},
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Venta', VentaSchema);