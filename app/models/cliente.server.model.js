'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;


var AvisoSchema = new Schema({
	mensaje: {
		type: String,
		default: '',
		trim: true
	},
	tipo: {
		type: String,
		trim: true
	},
	marca: {
		type: String,
		trim: true
	},
	fechaLimite: {
		type: String,
		trim: true
	}
});

/**
 * Cliente Schema
 */
var ClienteSchema = new Schema({
	nombre: {
		type: String,
		default: '',
		required: 'Ingrese el nombre del cliente',
		trim: true
	},
	nacimiento: {
		type: String,
		default: '',
		trim: true
	},
	telefono: {
		type: String,
		default: '',
		required: 'Ingrese el telefono del cliente',
		trim: true
	},
	email: {
		type: String,
		default: '',
		trim: true
	},
	direccion: {
		type: String,
		default: '',
		trim: true
	},
	talleRemera: {
		type: String,
		default: '',
		trim: true
	},
	talleJean: {
		type: String,
		default: '',
		trim: true
	},
	observaciones: {
		type: String,
		default: '',
		trim: true
	},
	intereses: {
		type: Array,
		default: '',
		trim: true
	},
	ciudad: {
		type: String,
		default: '',
		trim: true
	},
	avisos: [AvisoSchema],
	created: {
		type: Date,
		default: Date.now
	},
	user: {
		type: Schema.ObjectId,
		ref: 'User'
	}
});

mongoose.model('Cliente', ClienteSchema);