'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/**
 * Cliente Schema
 */
var MarcaSchema = new Schema({
	_id: {
		type: String,
		trim: true
	},
	nombre: {
		type: String,
		trim: true
	}
});

mongoose.model('Marca', MarcaSchema);