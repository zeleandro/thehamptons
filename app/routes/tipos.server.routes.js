'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var tipos = require('../../app/controllers/tipos.server.controller');

	// Marcas Routes
	app.route('/tipos')
		.get(tipos.list)
		.post(users.requiresLogin);
};