'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var marcas = require('../../app/controllers/marcas.server.controller');

	// Marcas Routes
	app.route('/marcas')
		.get(marcas.list)
		.post(users.requiresLogin);
};