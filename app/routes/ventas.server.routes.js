'use strict';

module.exports = function(app) {
	var users = require('../../app/controllers/users.server.controller');
	var ventas = require('../../app/controllers/ventas.server.controller');

	// Ventas Routes
	app.route('/ventas')
		.get(ventas.list)
		.post(users.requiresLogin, ventas.create);

	app.route('/ventas/:ventaId')
		.get(ventas.read)
		.put(users.requiresLogin, ventas.hasAuthorization, ventas.update)
		.delete(users.requiresLogin, ventas.hasAuthorization, ventas.delete);

	app.route('/ventasCliente/:vId')
		.get(ventas.ventasByCliente);

	// Finish by binding the Venta middleware
	app.param('ventaId', ventas.ventaByID);
	app.param('vId', ventas.ventasByCliente);
};
